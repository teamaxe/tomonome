/*
 *  AppComponent.cpp
 *  sdaProj
 */

#include "AppComponent.h"

AppComponent::AppComponent()
{
    meterValue = 0.f;
    
    addAndMakeVisible(&meterSlider);
    meterSlider.setSliderStyle(Slider::LinearBar);
    meterSlider.setRange(0.0, 1.0, 0.01);
    
    addAndMakeVisible(&deviceCombo);
    StringArray deviceList;
    monome.getDevicePathList(deviceList);
    deviceCombo.addItemList(deviceList, 1);
    
    monome.addListener(this);
    
    addAndMakeVisible(&connectButton);
    connectButton.addListener(this);
    
    audioDeviceManager.initialise(1, 2, 0, true, String::empty, 0);
    
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    audioDeviceManager.addAudioCallback(this);
    addActionListener(this);
    
    startTimer(100);
}
AppComponent::~AppComponent()
{
    audioDeviceManager.removeAudioCallback(this);
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

//ComponentCallbacks============================================================
void AppComponent::resized()
{
    int x = getWidth();
    int y = getHeight();
    
    meterSlider.setBounds(10, 10, x - 20, y/2 - 50);
    deviceCombo.setBounds(10, y/2, x - 20, 20);
    connectButton.setBounds(deviceCombo.getBounds().translated(0, 20));
}

//MidiInputCallback=============================================================
void AppComponent::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    if(message.isNoteOn(true))
	{
        String messageText;
        messageText << "MIDI NOTE MESSAGE:\t" << message.getChannel() << ":" << 
        message.getNoteNumber() << ":" << (int)message.getVelocity() << "\n";
        sendActionMessage(messageText);
		std::cout << messageText;
	}
	else if(message.isController())
	{
        String messageText;
        messageText << "MIDI CONTROL MESSAGE\t" << message.getChannel() << ":" << 
        message.getControllerNumber() << ":" << message.getControllerValue() << "\n";
		std::cout << messageText;
        sendActionMessage(messageText);
        if (message.getControllerNumber() == 23)
        {
            sendActionMessage("clear");
        }
    }
}

//AudioCallbacks================================================================
void AppComponent::audioDeviceIOCallback (const float** inputChannelData,
                                          int numInputChannels,
                                          float** outputChannelData,
                                          int numOutputChannels,
                                          int numSamples)
{
    const float *inL; //presume only mono input
    float *outL, *outR; 
    
    inL = inputChannelData[0]; 
    outL = outputChannelData[0]; 
    outR = outputChannelData[1];
    
    while(numSamples--) 
    {
        if (fabsf(*inL) > meterValue.get())
        {
            meterValue.set(fabsf(*inL));
        }
        else
        {
            meterValue.set(meterValue.get() * 0.9999f);
        }
        *outL = 0.f; //*inL;
        *outR = 0.f; //*inL;
        
        inL++; 
        outL++; 
        outR++;
    }
}

void AppComponent::audioDeviceAboutToStart (AudioIODevice* device)
{

}

void AppComponent::audioDeviceStopped()
{

}

void AppComponent::buttonClicked (Button* button)
{
    monome.setConnectionState(!connectButton.getToggleState(), deviceCombo.getText());
    connectButton.setToggleState(monome.isConnected(), false);
    
    //monome.getMonomeID();
    //monome.getMonomeGridSize();
}

//Action Callback
void AppComponent::actionListenerCallback (const String& message)
{
    if (message.startsWith("slider"))
    {
        String subString = message.fromFirstOccurrenceOf (":", false, true);
        float value = subString.getFloatValue();
        meterSlider.setValue(value);
    }

}

//Timer callback
void AppComponent::timerCallback()
{
    if (meterValue.get() < 0.5f)
         meterSlider.setColour(Slider::thumbColourId, Colours::green);
    else if (meterValue.get() < 0.8f)
        meterSlider.setColour(Slider::thumbColourId, Colours::orange);
    else
        meterSlider.setColour(Slider::thumbColourId, Colours::red);
        
    meterSlider.setValue(meterValue.get());
}

//MenuBarCallbacks==============================================================
StringArray AppComponent::getMenuBarNames()
{
	const char* const names[] = { "File", 0 };
	return StringArray (names);
}

PopupMenu AppComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
	PopupMenu menu;
	if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
	return menu;
}

void AppComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
	if (topLevelMenuIndex == FileMenu) 
    {
        if (menuItemID == AudioPrefs) 
        {
            AudioDeviceSelectorComponent audioSettingsComp (audioDeviceManager,
                                                            0, 2, 2, 2,
                                                            true, true, true, true);
            audioSettingsComp.setSize (500, 250);
            DialogWindow::showModalDialog ("Audio Settings", &audioSettingsComp, this, Colours::azure, true);
        }
    }
}


