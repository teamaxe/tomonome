/*
 *  JuceApp.cpp
 *  sdaProj
 */

#include "MainJuceApp.h"

JuceApp::JuceApp() : appWindow (0)
{

}

JuceApp::~JuceApp()
{

}

//==============================================================================
void JuceApp::initialise(const String& commandLine)
{
	appWindow = new AppWindow();
	appWindow->centreWithSize(600, 620);
	appWindow->setVisible(true);
}

void JuceApp::shutdown()
{
	deleteAndZero (appWindow);
}

//==============================================================================
void JuceApp::systemRequestedQuit()
{
	quit();
}

//==============================================================================
const String JuceApp::getApplicationName()
{
	return ProjectInfo::projectName;;
}

const String JuceApp::getApplicationVersion()
{	
	return ProjectInfo::versionString;
}

bool JuceApp::moreThanOneInstanceAllowed()
{
	return true;
}

void JuceApp::anotherInstanceStarted(const String& commandLine)
{

}

//==============================================================================
// This macro generates the main() routine that starts the app.
START_JUCE_APPLICATION(JuceApp)