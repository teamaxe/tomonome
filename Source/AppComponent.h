/*
 *  AppComponent.h
 *  sdaProj
 */

#ifndef H_APPCOMPONENT
#define H_APPCOMPONENT

#include "../JuceLibraryCode/JuceHeader.h"
#include "Monome/monome.h"

/**
 The main component for the applicaiton
 */
class AppComponent  :   public Component,
                        public MenuBarModel,
                        public MidiInputCallback,
                        public AudioIODeviceCallback,
                        public ActionBroadcaster,
                        public ActionListener,
                        public Timer,
                        public Button::Listener,
                        public Monome::Listener
                        

{	
public:
	//==============================================================================
    /**
     Constructor
     */
	AppComponent();
    
    /**
     Destructor
     */
	~AppComponent();
    
	//ComponentCallbacks============================================================
	void resized();
    
    //MidiInputCallback=============================================================
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message);
    
    //AudioCallbacks================================================================
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples);
    
    void audioDeviceAboutToStart (AudioIODevice* device);
    void audioDeviceStopped();
    
    //Action Callback
    void actionListenerCallback (const String& message);
    
    //timer callback
    void timerCallback();
    
    //button listen
    void buttonClicked (Button* button);
    
    void press (const unsigned int x, const unsigned int y, const bool state)
    {
        std::cout << x << "::" << y << "::" << state << "\n";
        
        //monome.setLed(x, y, state);
        
        monome.setLedRow (y, state == true ? 0xffff : 0);
        monome.setLedColumn (x, state == true ? 0xffff : 0);
    }
    
    
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
	{
		FileMenu=0,
		
		NumMenus
	};
    
    enum FileMenuItems 
	{
        AudioPrefs = 1,
		
		NumFileItems
	};
    StringArray getMenuBarNames();
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName);
	void menuItemSelected (int menuItemID, int topLevelMenuIndex); 
private:
	//==============================================================================
	//Add data members here
    //Audio
    AudioDeviceManager audioDeviceManager;
    
    //Serial
    Monome monome;
    
    //Components
    Slider meterSlider;
    ComboBox deviceCombo;
    TextButton connectButton;
    
    //Primitives 
    Atomic<float> meterValue;
};




#endif // H_APPCOMPONENT