/*
  ==============================================================================

    monome.h
    Created: 20 Apr 2013 8:50:31am
    Author:  tj3-mitchell

  ==============================================================================
*/

#ifndef H_MONOME
#define H_MONOME

#include "Serial/tlib_SerialPort.h"
#include "../../JuceLibraryCode/JuceHeader.h"

class Monome : SerialPort::Listener
{
public:
    Monome() : bufferIndex(0)
    {
        serialPort.addListener(this);
    }
    
    ~Monome()
    {
        setConnectionState(false);
        serialPort.removeListener(this);
    }
    
    /**
     Returns a list of monomes
     */
    int getDevicePathList (StringArray& devicePathList)
    {
        int retVal = SerialPort::getDevicePathList(devicePathList);
        int i = 0;
        while (i < devicePathList.size())
        {
            if (!devicePathList[i].contains("usbserial-m"))
            {
                devicePathList.remove(i);
                continue;
            }
            i++;
        }
        return retVal;
    }
    
    /**
     Set the connection state to the specifid monome device - returns false if something fails
     */
    int setConnectionState (const bool shouldBeRunning, const String&deviceName = String::empty)
    {
        if (serialPort.isReading())
        {
            serialPort.close();
        }
        else
        {
            if (serialPort.open (deviceName, SerialPort::Baud115200) != SerialPort::NoError)
                return false;
        }
        return true;
    }
    /**
     Returns the connection state
     */
    bool isConnected ()
    {
        return serialPort.isReading();
    }
    
    /**
     Set led
     */
    void setLed (const unsigned char x, const unsigned char y, bool state)
    {
        const unsigned int MessageSize = 2;
        unsigned char data[MessageSize];
        data[Data0] = 0x20 | (!state << 4);
        data[Data1] = (x << 4) | y;
        
        serialPort.write (0, data, MessageSize);
    }
    
    /**
     sets a row of leds
     */
    void setLedRow (const unsigned char row, const unsigned short state)
    {
        const unsigned int MessageSize = 3;
        unsigned char data[MessageSize];
        data[Data0] = 96 | (row);
        data[Data1] = state & 0xff;
        data[Data2] = state >> 8;
        
        serialPort.write (0, data, MessageSize);
    }
    
    /**
     sets a column of leds
     */
    void setLedColumn (const unsigned char column, const unsigned char state)
    {
        const unsigned int MessageSize = 2;
        unsigned char data[MessageSize];
        data[Data0] = 0x50 | (column);
        data[Data1] = state;
        
        serialPort.write (0, data, MessageSize);
    }
    
    /**
     gets the device ID
     */
    void getMonomeID ()
    {
        const unsigned int MessageSize = 1;
        unsigned char data[MessageSize];
        data[Data0] = 0x00;
        
        serialPort.write (0, data, MessageSize);
    }
    
    /**
     gets the device ID string
     */
    void getMonomeIDString ()
    {
        const unsigned int MessageSize = 1;
        unsigned char data[MessageSize];
        data[Data0] = 0x01;
        
        serialPort.write (0, data, MessageSize);
    }
    
    /**
     gets the grid size
     */
    void getMonomeGridSize ()
    {
        const unsigned int MessageSize = 1;
        unsigned char data[MessageSize];
        data[Data0] = 0x02;
        
        serialPort.write (0, data, MessageSize);
    }
    
    
    ////////////////Listeners
	/**
     Class for a listener to this monome
     */
	class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener()                                     {}
		
        /** Called when the a data packet is recieved. */
        virtual void press (const unsigned int x, const unsigned int y, const bool state) = 0;
    };
	
    /**
     adds the listener to recieve bytes as they are recieved. Only one at a time at the moment.
     */
	void addListener(Monome::Listener* const listenerToAdd)
    {
        if (listenerToAdd != 0)
        {
            ScopedLock sl(listenerLock);
            listeners.add(listenerToAdd);
        }
    }
    
    /**
     removes the listener so that it will no londger recieve messages.
     */
	void removeListener(Monome::Listener* const listenerToRemove)
    {
        if (listenerToRemove != 0)
        {
            ScopedLock sl(listenerLock);
            listeners.remove(listenerToRemove);
        }
    }
    
    //SerialPort
    void serialDataRecieved (const int portIndex, const uint8 byte)
    {
        buffer[bufferIndex++] = byte;
        if (bufferIndex == RecieveBufferSize)
        {
            bufferIndex = 0;
            parse (buffer);
        }
        //serialPort.writeToPort(0, &byte, 1);
        printf("%d ", byte);
        //std::cout << portIndex << ":" << byte << "\n";
    }
    
    void serialConnectionError(const unsigned int errorCode)
    {
        std::cout << "error:" << errorCode << "\n";
    }
    
private:
    
    /**
     parse the raw data to decode messages
     */
    void parse (const unsigned char buffer[2])
    {
        unsigned char section = buffer[Data0] & 0xf0;
        unsigned char command = buffer[Data0] & 0x0f;
        
        if (section == 0x00 || section == 0x10) //press or release
        {
            ScopedLock sl (listenerLock);
            listeners.call (&Listener::press, (buffer[Data1] >> 4), buffer[Data1] & 0x0f, !(section >> 4));
            //std::cout << (buffer[Data1] >> 4) << ":" <<  << ":" << (section >> 4) << "\n";
        }
    }
    
    //constants
    enum 
    {
        RecieveBufferSize = 2,
        Data0 = 0,      //first byte
        Data1 = 1,      //second byte
        Data2 = 2,      //third byte
    };
    
    //DeviceType
    enum
    {
        Monome128 = 0,
        Arc4,
    };
    
    SerialPort serialPort;
    
    unsigned char buffer[RecieveBufferSize];
    unsigned char bufferIndex;
    
    CriticalSection listenerLock;
    ListenerList<Monome::Listener> listeners;
};

#endif  // H_MONOME
