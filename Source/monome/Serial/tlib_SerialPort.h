/*
 *  tlib_SerialPort.h
 *  sdaJuce
 *
 *  Created by tjmitche on 19/05/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef H_TLIB_SERIALPORT
#define H_TLIB_SERIALPORT

////////////

#include "../../../JuceLibraryCode/JuceHeader.h" 
#include <iostream>
#include <string>

#include <fcntl.h>
#include <termios.h>

/**
 Class to manage a serial port connection. 
 
 
 Todo: 
 - fine control of the serial port settings!
 - thread saftey. fd should be atomic and variables accessed in functions that
 can be called on any thread should be sorted out.
 - When reading enable timeout, bufferlength or timeout to control when callbacks are made.
 - Make a windows port, see these links
 -- http://msdn.microsoft.com/en-us/library/aa363201(VS.85).aspx
 -- http://cboard.cprogramming.com/windows-programming/141173-windows-serial-programming.html
 -- http://www.robbayer.com/files/serial-win.pdf
 -- http://www.programmersheaven.com/mb/windows/306436/306436/c-serial-port-programming-in-windows-using-visual-c++60/
 -- https://www.google.co.uk/search?q=serial+port+C%2B%2B+windows+code&oq=serial+port+C%2B%2B+windows+code&aqs=chrome.0.57j0j62l3.6498&sourceid=chrome&ie=UTF-8
 
 - add control for nonstandard baud rates.
 
 */

class SerialPort : public Thread 
{
public:
    /**
     Enum for error codes. Error descriptions can be pulled from @getError()
     */
    enum ErrorCode
    {
        NoError = 0,
        FindDeviceError,
        BuildDeviceListError,
        ReadThreadExitError,
        PortWriteError,
        PortWriteClosedError,
        OpenPortError,
        SelectError,
        SelectTimeoutError,
        ReadInputError,
        NoInputAvailableError,
        UnknownPortNameError,
        AlreadyRunningError,
        NoPortsOpenError,
        ThreadRunningError,
        ClosePortError,
        
        NumErrors
    };
    
    /**
     function for reading the error description
     */
    static std::string getError(const unsigned int errorCode)
    {
        if(errorCode >= NumErrors)
            return "";
        
        static const char* errorString[NumErrors] = 
        {
            "SerialPort: No Error",
            "SerialPortError: IO Services or Kernel error while trying to find serial devices",
            "SerialPortError: Error building RS232 device path list",
            "SerialPortError: Unable to cleanly exit the read thread - forcibly killed and port closed",
            "SerialPortError: Writing to the Serial Port Failed",
            "SerialPortError: Cannont write to the port as it is closed",
            "SerialPortError: Unable to open port",
            "SerialPortError: An error occured, waiting for port activity (select() error)",
            "SerialPortError: Serial port timeout - no serial port activity",
            "SerialPortError: Error reading from the serial input",
            "SerialPortError: Read data not available - (select() error?)",
            "SerialPortError: Unknown port name",
            "SerialPortError: Serial ports are already running",
            "SerialPortError: No open ports",
            "SerialPortError: Cannot open or close ports while read thread is running",
            "SerialPortError: Unable to close port"
        };
        return errorString[errorCode];
    }
    
    /**
     Populates the provided list with all availabe Serial (RS232) ports on the machine. 
     
     If anything goes wrong it will return a non-zero @SerialPort::ErrorCode
     */
    static unsigned int getDevicePathList (StringArray& devicePathList);
    
    /** Enum to specify the baud rate for the port */
    enum BaudRate
	{
		Baud2400    = B2400,
		Baud4800    = B4800,
		Baud9600    = B9600,
        Baud19200   = B19200,
        Baud38400   = B38400,
        Baud115200  = B115200,
        Baud230400  = B230400
	};
    
    /** Enum to specify the read write access for the port */
	enum ReadWriteAccess 
	{
		ReadOnly        = O_RDONLY,
		WriteOnly       = O_WRONLY,			
		ReadAndWrite    = O_RDWR
	};
    
    /** Enum for parity settings */
    enum Parity
	{
		ParityOff    = 0,
		ParityEven,
		ParityOdd
	};
    
    /** Enum to specify the flowcontrol for the port */
    enum FlowControl 
    {
        
    };
    
    /**
     A storage class for all the settings of a serial port connection. Constrcuted with the default settings.
     */
    class PortSettings
    {
    public:
        PortSettings () :   descriptor (-1),
                            baudRate (Baud115200),
                            readWriteAccess(ReadAndWrite),
                            parity (ParityOff),
                            hardwareFlowControlEnabled (false),
                            softwareFlowControlEnabled (false)
        {
            
        }
        String portName;                    
        int descriptor;                     //for the unix file descriptor
        BaudRate baudRate;                  
        ReadWriteAccess readWriteAccess;    
        Parity parity;                 
        bool hardwareFlowControlEnabled;
        bool softwareFlowControlEnabled;
    };

    static const PortSettings defaultSettings;
	
    /** Constructor */
	SerialPort();
    
    /** Destructor */
	~SerialPort();
    
    /** Opens a port with the specified port settings.
     
     This can be called multiple times to open multiple ports. If the readWriteAccess member is set to ReadOnly or ReadAndWrite then the read thread will be started if it not already running.
     
     Multiple opens can be made to different ports and they may be accessed by an index in the
     order they are added, i.e. the first port to be opened is index 0, the second is index 1 and so forth
     
     If anything goes wrong it will return a non-zero @SerialPort::ErrorCode */
    unsigned int open (PortSettings& settings);
    
    /** Opens the port with the specified name and baudrate. The rest of the settings take the default @PortSettings values. */
    unsigned int open (const String& portName, const BaudRate baudRate);

    /** Closes the specified connection number. */
    unsigned int close (const unsigned int connectionNumber);
    
    /** Closes all connections */
    void close();
    
    /** returns true if the specified port is open */
    bool isOpen (const unsigned int connectionNumber);
    
    /** returns true if any ports are open */
    bool isOpen ();
    
    /** write data to the port.
     If anything goes wrong it will return a non-zero @SerialPort::ErrorCode */
    unsigned int write (const unsigned int portNumber, const unsigned char* const data, unsigned int dataSize);
    
    /** Returns the read state - if we are reading from the ports then this will be true. */
    bool isReading() const;
	
    ////////////////Listeners
	/** Class for a listener to this serial port connection to inherit. Only one listener at present. */
	class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener()                                     {}
		
        /** Called when the a data packet is recieved. */
        virtual void serialDataRecieved (const int portIndex, const uint8 byte) = 0;
        /** Called when the connection fails */
        virtual void serialConnectionError(const unsigned int errorCode) = 0;
    };
	
    /** adds the listener to recieve bytes as they are recieved. Only one at a time at the moment. */
	void addListener(SerialPort::Listener* const listenerToAdd);
	
    /** removes the listener so that it will no londger recieve messages. */
	void removeListener(SerialPort::Listener* const listenerToRemove);
    
private:
    
    /** Starts the read thread to read data from all open ports. Recieved data will be sent on to all
     registered listeners.
     
     If anything fails it will return a non-zero @SerialPort::ErrorCode */
    unsigned int setReadState(bool shouldBeReading);
    
    /** read a byte of raw data.
     
     If anything goes wrong it will return a non-zero @SerialPort::ErrorCode */
    unsigned int readByte(unsigned char *byte);
	
    /** Run funciton for the thread. When this function exits the port is closed and the read thread stops. */    
	void run();
    

    //OSX port data
    CriticalSection portSettingsLock;
    Array<PortSettings> portSettings;
    int descriptorMax;                  //
    
    //////////
    CriticalSection listenerLock;
	ListenerList<Listener> listeners; //for our only listener
	
	JUCE_LEAK_DETECTOR(SerialPort);
    SerialPort (const SerialPort&);
    SerialPort& operator= (const SerialPort&);
};

#endif // H_TLIB_SERIALPORT