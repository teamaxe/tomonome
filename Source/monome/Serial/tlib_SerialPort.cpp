/*
 *  tlib_SerialPort.cpp
 *  sdaJuce
 *
 *  Created by tjmitche on 19/05/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "tlib_SerialPort.h"
#include <IOKit/IOKitLib.h>
#include <IOKit/serial/IOSerialKeys.h>
#include <IOKit/serial/ioss.h>
#include <sys/ioctl.h>
#include <sys/param.h>

unsigned int SerialPort::getDevicePathList (StringArray& devicePathList)
{
    io_iterator_t serialPortIterator;
    
    CFMutableDictionaryRef classesToMatch;
    
    classesToMatch = IOServiceMatching (kIOSerialBSDServiceValue);
    if (classesToMatch == 0)
        return FindDeviceError;
    
    CFDictionarySetValue (classesToMatch, CFSTR(kIOSerialBSDTypeKey), CFSTR(kIOSerialBSDRS232Type));
    
    kern_return_t kernResult; 
    kernResult = IOServiceGetMatchingServices (kIOMasterPortDefault, classesToMatch, &serialPortIterator);    
    if (kernResult != KERN_SUCCESS)
        return FindDeviceError;
    
    io_object_t	deviceService;
    
    // Loop untill we have all the devices
    while ((deviceService = IOIteratorNext (serialPortIterator)))
    {
		// Get the callout device's path (/dev/cu.xxxxx).         
		CFTypeRef bsdPathAsCFString = IORegistryEntryCreateCFProperty ( deviceService,
                                                                       CFSTR(kIOCalloutDeviceKey),
                                                                       kCFAllocatorDefault, 0);
        if (bsdPathAsCFString == NULL) 
            return BuildDeviceListError;
        
        Boolean result;
        char bsdPath[MAXPATHLEN] = {'\0'};
        
        // Convert the path from a CFString to a C (NUL-terminated) string for use with the POSIX open() call.
        result = CFStringGetCString ((CFStringRef)bsdPathAsCFString,
                                     bsdPath,
                                     MAXPATHLEN, 
                                     kCFStringEncodingUTF8);
        CFRelease (bsdPathAsCFString);
        
        if (result == false)
            return BuildDeviceListError;
        
        devicePathList.add (bsdPath);
        //printf ("Device found with BSD path: %s\n", bsdPath);
        
        // Release the io_service_t now that we are done with it.
		(void) IOObjectRelease (deviceService);
    }
    
    IOObjectRelease(serialPortIterator);	// Release the iterator.
    return NoError;
}

SerialPort::SerialPort()    :   Thread("Serial Connection")
{

}

SerialPort::~SerialPort()
{
    if (isReading()) 
        setReadState(false);

    //if any ports are open then close them!
    //closeAllPorts();
    close();
}

unsigned int SerialPort::open (PortSettings& settings)
{
    
    StringArray validDeviceList;
    getDevicePathList(validDeviceList);
    
    if (validDeviceList.contains(settings.portName) == false)
    {
        return UnknownPortNameError;
    }
    
    settings.descriptor = ::open(settings.portName.toUTF8(), settings.readWriteAccess | O_NOCTTY);
    
    if (settings.descriptor == -1) //did this fail?
    {
        DBG("Unable to open serial port\n");
        return OpenPortError;
    }
    
    //    // don't allow multiple opens
    //    if (ioctl(descriptor, TIOCEXCL) == -1)
    //    {
    //        //DBG("SerialPort::open : ioctl error, non critical");
    //        closePort();
    //        return OpenPortError;
    //    }
    
    //
    //    if (fcntl(descriptor, F_SETFL, 0) == -1)
    //    {
    //        //DBG("SerialPort::open : fcntl error");
    //		closePort();
    //        return OpenPortError;
    //    }
    
    //    struct termios options;
    //    memset(&options, 0, sizeof(struct termios));
    //    if(tcgetattr(portDescriptor, &options) == -1)	//Get the current options for the port...
    //    {
    //        //DBG("SerialPort::open : can't get port settings to set timeouts");
    //        closePort();
    //        return OpenPortError;
    //    }
    
    struct termios options;
    memset(&options, 0, sizeof(struct termios));
    
    //non canocal, 0.5 second timeout, read returns as soon as any data is recieved
    
    cfmakeraw(&options);
    options.c_cc[VMIN] = 0;     //read does not wait for any chars it returs immediately with whatever is available
    options.c_cc[VTIME] = 0;    //read returns immediately and does not wait. - we are using select to wait.
	options.c_cflag |= CREAD;   //enable reciever (daft) // this is set by cfmakeraw anyway
    options.c_cflag |= CLOCAL;   //this prevents us from becoming the port owner of the port
    
    //Baud
	cfsetspeed(&options, settings.baudRate);
    
    //Bits
    options.c_cflag |= CS8; //8 data bits - this is set by default
    
    
    //Parity
    if (settings.parity == ParityOff)
    {
        options.c_cflag &= ~PARENB;
    }
    else 
    {
        options.c_cflag |= PARENB;
        if (settings.parity == ParityOdd)
            options.c_cflag |= PARODD;
    }
  
    
    //STOP
    //options.c_cflag &= ~CSTOPB;         //disable 2 stop bits
    
    //character size mask??
    //options.c_cflag &= ~CSIZE;
    
    //FLOWCONTROL XONXOFF:
    //options.c_iflag |= IXON;
    //options.c_iflag |= IXOFF;
    
    //FLOWCONTROL_HARDWARE:
    //options.c_cflag |= CCTS_OFLOW;
    //options.c_cflag |= CRTS_IFLOW;
    //options.c_cflag |= CRTSCTS;    // enable hardware flow control
    //options.c_cflag &= ~CRTSCTS;	//dissable hardware flow control dissabled by default
    
    
    //options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);	//recieve unprocessed raw input
    //options.c_iflag &= ~(IXON | IXOFF | IXANY);	// enabled software flow control
    //options.c_iflag &= ~(IXON | IXOFF | IXANY);	//dissable software flow control
    
    //makes no difference
    if (tcsetattr (settings.descriptor, TCSAFLUSH, &options) == -1)
    {
        DBG ("SerialPort::open : can't set port settings (timeouts)");
		::close(settings.descriptor);
        return OpenPortError;
    }
    
    //----------------
    //    if (tcsetattr(descriptors[1], TCSAFLUSH, &options) == -1)
    //    {
    //        //DBG("SerialPort::open : can't set port settings (timeouts)");
    //		closePort();
    //        return OpenPortError;
    //    }
    //    if (tcsetattr(descriptors[2], TCSAFLUSH, &options) == -1)
    //    {
    //        //DBG("SerialPort::open : can't set port settings (timeouts)");
    //		closePort();
    //        return OpenPortError;
    //    }
    //----------------
    
    if (tcsetattr (settings.descriptor,TCSANOW,&options) == -1)
    {
        std::cout << "SerialPort::open : can't set port settings (timeouts)";
		::close(settings.descriptor);
        return OpenPortError;
    }
    
    //-------------
    //    if (tcsetattr(descriptors[1],TCSANOW,&options) == -1)
    //    {
    //        //DBG("SerialPort::open : can't set port settings (timeouts)");
    //		closePort();
    //        return OpenPortError;
    //    }
    //    if (tcsetattr(descriptors[2],TCSANOW,&options) == -1)
    //    {
    //        //DBG("SerialPort::open : can't set port settings (timeouts)");
    //		closePort();
    //        return OpenPortError;
    //    }
    //-------------
    
    //    descriptorMax = -1;
    //    for (int i = 0; i > descriptors.size(); i++)
    //    {
    //        if (descriptors[i] > descriptorMax)
    //            descriptorMax = descriptors[i];
    //    }
    
    
    //set non standard baud rate
    //    speed_t nonStandardBaud = 250000;
    //    if (ioctl(descriptors.getLast(), IOSSIOSPEED, &nonStandardBaud, 1) == -1)
    //    {
    //        std::cout << "SerialPort::open : can't set non-standard speed";
    //        closePort(descriptors.size()-1);
    //        return OpenPortError;
    //    }
    
    //scope for the lock
    {
        ScopedLock sl(portSettingsLock);
        portSettings.add(settings);
        descriptorMax = -1;
        for (int i = 0; i < portSettings.size(); i++)
            if (portSettings[i].descriptor > descriptorMax)
                descriptorMax = portSettings[i].descriptor;
        descriptorMax += 1;
    }
    
    if (settings.readWriteAccess == ReadOnly || settings.readWriteAccess == ReadAndWrite)
    {
        setReadState(true);
    }
    
    return NoError;//success
}

unsigned int SerialPort::open (const String& portName, const BaudRate baudRate)
{
    PortSettings ps;
    ps.portName = portName;
    ps.baudRate = baudRate;
    
    return open(ps);
}

unsigned int SerialPort::close (const unsigned int connectionNumber)
{
    if(isOpen (connectionNumber))
    {
        bool stillReading = false; //this is to see if there are any ports still reading
        {//lock scope
            ScopedLock sl(portSettingsLock);
            if(::close(portSettings[connectionNumber].descriptor) == -1)					// Close the Serial Port
                return ClosePortError;
            else
                portSettings.remove(connectionNumber);

            //check to see if there are any reading ports open.
            for (int i = 0; i < portSettings.size(); i++)
                if (portSettings[i].readWriteAccess != WriteOnly)
                    stillReading = true;
        }
        
        //if no reading ports are open then shut down the read thread
        if (isThreadRunning() && stillReading == false)
        {
            return setReadState(false);
        }
    }
    
    return NoError;
}

bool SerialPort::isOpen (const unsigned int connectionNumber)
{
    if (connectionNumber < portSettings.size())
        return true;
    
    return false;
}

bool SerialPort::isOpen ()
{
    return portSettings.size() > 0;
}

void SerialPort::close()
{
    if (isThreadRunning())
    {
         stopThread(100);   //this should be enough time for the read thread to exit.
    }
    
    for (int i = 0; i < portSettings.size(); i++)
    {
        close (i);
    }
}

unsigned int SerialPort::setReadState(bool shouldBeReading)
{
    if (shouldBeReading) 
    {
        if (isThreadRunning()) 
        {
            return AlreadyRunningError;
        }
        startThread(6);     //if all is well then start her up!
    }
    else 
    {
        signalThreadShouldExit();
        if (waitForThreadToExit(500) == false) //if we exited cleanly
        {
            stopThread(0);
            return ReadThreadExitError;
        }
    }
    return NoError;
}

bool SerialPort::isReading() const
{ 
    return isThreadRunning();
}

unsigned int SerialPort::write (const unsigned int portNumber, const unsigned char* const data, unsigned int dataSize)
{
    if (isOpen(portNumber))
    {
        if (::write(portSettings[portNumber].descriptor, data, dataSize) == dataSize)
            return NoError;
        else
            return PortWriteError;
    }
    return PortWriteClosedError;
}

void SerialPort::run()
{
    DBG ("Starting Read Thread");
    while (!threadShouldExit())
    {
        unsigned char dataByte;
        unsigned int errorCode;// = readByte(&data);
        ////////////////////////////////////
        /* Do the select */
        struct timeval timeout;
        timeout.tv_sec  = 0;
        timeout.tv_usec = 50000;		//twentith of a second
        fd_set  descriptorSet;
        FD_ZERO(&descriptorSet);
        
        {
            ScopedLock sl (portSettingsLock);
            for (int i = 0; i < portSettings.size(); i++)
                FD_SET(portSettings[i].descriptor, &descriptorSet);
        }
        
        int ret  = select(descriptorMax, &descriptorSet, NULL, NULL, &timeout);
        
        if (ret < 0)
        {
            std::cout << "SerialPort: select failed\n";
            errorCode = SelectError;
        }
        else if (ret == 0)
        {
            //std::cout << "SerialPort: Data timeout?\n";
            //        break;
            errorCode = SelectTimeoutError;
        }
        else //select worked fine
        {
            ScopedLock sl (portSettingsLock);
            for (int i = 0; i < portSettings.size(); i++)
            {
                /* If we have input then read it */
                if (FD_ISSET(portSettings[i].descriptor, &descriptorSet))
                {
                    int readSize = read (portSettings[i].descriptor, &dataByte , 1);
                    if (readSize != 1) //if it didn't work
                    {
                        std::cout << i << "serial read error:" << strerror (errno) << " readSize:" << readSize << "\n";
                        errorCode = ReadInputError;
                    }
                    else
                    {
                        ScopedLock sl (listenerLock);
                        listeners.call (&Listener::serialDataRecieved, i, dataByte);
                    }
                }
                else
                {
                    errorCode = NoInputAvailableError;
                }
            }
        }
        errorCode = NoError;
    }
}

void SerialPort::addListener(SerialPort::Listener* const listenerToAdd)
{
    if (listenerToAdd != 0) 
    {
        ScopedLock sl(listenerLock);
        listeners.add(listenerToAdd);
    }
}

void SerialPort::removeListener(SerialPort::Listener* const listenerToRemove)
{
    if (listenerToRemove != 0) 
    {
        ScopedLock sl(listenerLock);
        listeners.remove(listenerToRemove);
    }
}

