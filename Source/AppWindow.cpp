/*
 *  JuceWindow.cpp
 *  sdaProj
 */

#include "AppWindow.h"
#include "AppComponent.h"

//==============================================================================
AppWindow::AppWindow()
: 
DocumentWindow(
				"sdaAudioMidi",                   // Set the text to use for the title
				Colours::azure,					// Set the colour of the window
				DocumentWindow::allButtons,		// Set which buttons are displayed
				true							// This window should be added to the desktop
			  )
{
    setResizable(true, false); 
	setTitleBarHeight(20); 
    AppComponent* appComponent = new AppComponent();
    setContentOwned(appComponent, false);
    setMenuBar (appComponent);
}

AppWindow::~AppWindow()
{
 
}

void AppWindow::closeButtonPressed()
{
    JUCEApplication::getInstance()->systemRequestedQuit();
} 